# 3.2. Compare original weights used to calculate the minor allele and sample reads in the simulation,
# to the estimated weights
# Input: original and estimated weights
# Output: some plots

library(tidyverse)
library(ggpubr)
library(patchwork)

samples = c("pool_8d",
            "pool_19d",
            "pool_20d_similar")
trials = 50

for (sample in samples) {
  estimated_donor_proportions_balance = read.table(
    paste0(
      "../../data/",
      sample,
      "/b_exon/w_estimate_donor_proportions_balance_",
      trials,
      "_trials.txt.gz"
    )
  )
  donor_proportions_balance =  read.table(
    paste0(
      "../../data/",
      sample,
      "/b_exon/donor_proportions_balance_",
      trials,
      "_trials.txt.gz"
    )
  )
  
  estimated_to_plot = estimated_donor_proportions_balance %>%
    pivot_longer(cols = c(-coverage, -trial),
                 names_to = c("donor"))
  
  # original proportions to long format
  donor_proportions_balance$trial = 1:nrow(donor_proportions_balance)
  original_to_plot = donor_proportions_balance %>%
    pivot_longer(cols = -trial, names_to = c("donor"))
  
  to_plot = inner_join(estimated_to_plot, original_to_plot, by = c("trial", "donor"))
  
  to_plot = rename(to_plot, w_estimated = value.x, w_original = value.y)
  
  to_plot$w_diff = to_plot$w_original - to_plot$w_estimated
  
  #scatter
  scatter_with_regression = function(to_plot) {
    p = ggscatter(
      to_plot,
      x = "w_original",
      y = "w_estimated",
      add = "reg.line",
      # Add regressin line
      add.params = list(color = "blue", fill = "lightgray"),
      # Customize reg. line
      conf.int = TRUE # Add confidence interval
    ) +
      
      stat_cor(aes(label = ..r.label..), label.x = 0.04) +
      geom_abline(slope = 1, intercept = 0) +
      ggtitle(unique(to_plot$coverage))
    # Add correlation coefficient
    
    
    return(p)
  }
  
  p1 = scatter_with_regression(to_plot[to_plot$coverage == "0.1", ])
  p2 = scatter_with_regression(to_plot[to_plot$coverage == "1", ])
  p3 = scatter_with_regression(to_plot[to_plot$coverage == "2.5", ])
  p4 = scatter_with_regression(to_plot[to_plot$coverage == "5", ])
  p5 = scatter_with_regression(to_plot[to_plot$coverage == "10", ])
  p6 = scatter_with_regression(to_plot[to_plot$coverage == "20", ])
  p7 = scatter_with_regression(to_plot[to_plot$coverage == "50", ])
  
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_balance_estimate_vs_original.png"
    ),
    width = 8,
    height = 10,
    units = "in",
    res = 400
  )
  print((p1 + p2 + p3) / (p4 + p5 + p6) / (p7 + patchwork::plot_spacer() + patchwork::plot_spacer()))
  dev.off()
  # ggplot(aes(x=means, y=sigmas, col=CVs, size=CVs), data=df) + geom_point()
  # Boxplot
  p <-
    ggplot(to_plot, aes(x = as.factor(coverage), y = abs(w_diff))) + geom_boxplot() +
    ylab("|original w - estimated w|") + xlab("Coverage") + theme_bw()
  p
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_balance_absDiff_original-estimate.png"
    ),
    width = 4,
    height = 4,
    units = "in",
    res = 400
  )
  print(p)
  dev.off()
  
  
  # correlation per trial
  gp = to_plot %>%
    group_by(coverage, trial) %>%
    summarize(correlation = cor(w_estimated, w_original))
  
  p = ggplot(gp, aes(x = as.factor(coverage), y = correlation)) +
    geom_boxplot() +
    geom_dotplot(binaxis = 'y',
                 stackdir = 'center',
                 dotsize = 0.5) +
    ylab("Correlation original vs estimated w (per trial)") +
    xlab("Coverage") + theme_bw()
  
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_balance_correlation_per_trial.png"
    ),
    width = 4,
    height = 4,
    units = "in",
    res = 400
  )
  print(p)
  dev.off()
  
  
  ## now for unbalanced proportions
  estimated_donor_proportions_imbalance = read.table(
    paste0(
      "../../data/",
      sample,
      "/b_exon/w_estimate_donor_proportions_imbalance_",
      trials,
      "_trials.txt.gz"
    )
  )
  donor_proportions_imbalance =  read.table(
    paste0(
      "../../data/",
      sample,
      "/b_exon/donor_proportions_imbalance_",trials,"_trials.txt.gz"
    )
  )
  
  estimated_to_plot = estimated_donor_proportions_imbalance %>%
    pivot_longer(cols = c(-coverage, -trial),
                 names_to = c("donor"))
  
  # original proportions to long format
  donor_proportions_imbalance$trial = 1:nrow(donor_proportions_imbalance)
  original_to_plot = donor_proportions_imbalance %>%
    pivot_longer(cols = -trial, names_to = c("donor"))
  
  to_plot = inner_join(estimated_to_plot, original_to_plot, by = c("trial", "donor"))
  
  to_plot = rename(to_plot, w_estimated = value.x, w_original = value.y)
  
  to_plot$w_diff = to_plot$w_original - to_plot$w_estimated
  
  
  
  p1 = scatter_with_regression(to_plot[to_plot$coverage == "0.1", ])
  p2 = scatter_with_regression(to_plot[to_plot$coverage == "1", ])
  p3 = scatter_with_regression(to_plot[to_plot$coverage == "2.5", ])
  p4 = scatter_with_regression(to_plot[to_plot$coverage == "5", ])
  p5 = scatter_with_regression(to_plot[to_plot$coverage == "10", ])
  p6 = scatter_with_regression(to_plot[to_plot$coverage == "20", ])
  p7 = scatter_with_regression(to_plot[to_plot$coverage == "50", ])
  
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_imbalance_estimate_vs_original.png"
    ),
    width = 8,
    height = 10,
    units = "in",
    res = 400
  )
  print((p1 + p2 + p3) / (p4 + p5 + p6) / (p7 + patchwork::plot_spacer() + patchwork::plot_spacer()))
  dev.off()
  
  # Boxplot
  p <-
    ggplot(to_plot, aes(x = as.factor(coverage), y = abs(w_diff))) + geom_boxplot() +
    ylab("|original w - estimated w|") + xlab("Coverage") + theme_bw()
  p
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_imbalance_absDiff_original-estimate.png"
    ),
    width = 4,
    height = 4,
    units = "in",
    res = 400
  )
  print(p)
  dev.off()
  
  # correlation per trial
  gp = to_plot %>%
    group_by(coverage, trial) %>%
    summarize(correlation = cor(w_estimated, w_original))
  
  p = ggplot(gp, aes(x = as.factor(coverage), y = correlation)) +
    geom_boxplot() +
    geom_dotplot(binaxis = 'y',
                 stackdir = 'center',
                 dotsize = 0.5) +
    ylab("Correlation original vs estimated w (per trial)") +
    xlab("Coverage") + theme_bw()
  
  png(
    paste0(
      "../../data/",
      sample,
      "/proportions/exome-seq/donor_proportions_",
      trials,
      "_trials_imbalance_correlation_per_trial.png"
    ),
    width = 4,
    height = 4,
    units = "in",
    res = 400
  )
  print(p)
  dev.off()
}