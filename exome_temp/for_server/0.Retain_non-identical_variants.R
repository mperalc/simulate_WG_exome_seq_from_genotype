# Filter VCF to only the variants that are different across all donors
.libPaths("/lustre/scratch117/cellgen/teamtrynka/marta/bin/R/R-4.0.0/lib")

library(vcfR)
library(data.table)
library(tidyverse)
options(stringsAsFactors = FALSE)
gc()


# For server
args = commandArgs(trailingOnly=TRUE)


if (length(args)<2) {
  stop("You need to detail input and output file path.n", call.=FALSE)
} else if (length(args)==2) {
  vcf_path = args[1]
  output_path = args[2]
  
}

# Function to convert gt vcf to minor allele dosages
process_gt_dosages = function(vcf){
  # Excluding indels (subsetting only to SNPs) - we may want to keep this info for calculating the proportions?
  # vcf2 <- extract.indels(vcf)
  
  ## Extracting GT
  gt <- as.data.table(extract.gt(vcf))
  # first make new names from chromosome, position, ref and alt (so there are no duplicates)
  names = paste0(vcf@fix[,"CHROM"],"_",vcf@fix[,"POS"],"_",vcf@fix[,"REF"],"_",vcf@fix[,"ALT"])
  
  # there are still some duplicates so remove those
  gt=gt[!duplicated(names),]
  message("Removing ",sum(duplicated(names)), " duplicated rows from vcf")
  
  ## removing those from the names
  names=names[!duplicated(names)]
  gt[, ("rn") := names] 
  
  # setkey(gt,rn) # this sorts the data.table, be careful
  object.size(gt)
  
  ### Some meanings: ################
  # / : genotype unphased (e.g. 0/0)
  #  | : genotype phased (e.g. 0|0)
  # Phased data are ordered along one chromosome and so from these data you know the haplotype
  # i.e. the variants are known to be on the same homologous chromosome because some reads were found to carry both variants
  # Unphased data are simply the genotypes without regard to which one of the pair of chromosomes holds that allele
  ################
  
  
  ## converting GT to minor allele score
  
  gt2 =  gt %>%
    mutate_all(funs(str_replace_all(., "0\\|0", "0")))
  
  gt2 =  gt2 %>%
    mutate_all(funs(str_replace_all(., "0\\/0", "0")))
  
  gt2 = gt2 %>%
    mutate_all(funs(str_replace_all(., "0\\|1", "0.5")))
  
  gt2 = gt2 %>%
    mutate_all(funs(str_replace_all(., "1\\|0", "0.5")))
  
  gt2 =  gt2 %>%
    mutate_all(funs(str_replace_all(., "0\\/1", "0.5")))
  gt2 =  gt2 %>%
    mutate_all(funs(str_replace_all(., "1\\/0", "0.5")))
  
  gt2 = gt2 %>%
    mutate_all(funs(str_replace_all(., "1\\|1", "1")))
  gt2 = gt2 %>%
    mutate_all(funs(str_replace_all(., "1\\/1", "1")))
  
  # change donor columns to numeric
  cols = setdiff(colnames(gt2),"rn")
  gt2[ ,(cols) := lapply(.SD, as.numeric),.SDcols = cols] 
  
  # hist(gt2)
  ## Some donors have NAs at some SNP positions
  anyNA(gt2)
  gt2[which(rowSums(is.na(gt2)) != 0)[1], ]
  gt[which(rowSums(is.na(gt2)) != 0)[1], ]
  
  # Removing SNPs with NAs in at least one donor - do I need to do this? Can I ignore the NAs somehow?
  gt2 = gt2[which(rowSums(is.na(gt2)) == 0), ]
  return(gt2)
}

message("...Reading VCF...")


vcf = vcfR::read.vcfR(vcf_path)

message("...Calculating minor allele dosages...")

gt2 = process_gt_dosages(vcf)

# Get rows of non-identical alleles

cols <-c(setdiff(colnames(gt2),"rn"))

non_identical_rows = apply(gt2[,..cols], 1,function(x) length(unique(x))>1)

original_length = nrow(gt2)
gt2 = gt2[non_identical_rows,]
non_identical_genotype_length = nrow(gt2)

## subset vcf by identifying remaining snps
ids = paste0(vcf@fix[,"CHROM"],"_",vcf@fix[,"POS"],"_",vcf@fix[,"REF"],"_",vcf@fix[,"ALT"])
tosubset = ids %in% gt2$rn
vcf@gt = vcf@gt[tosubset,]
vcf@fix = vcf@fix[tosubset,]
vcf@meta = append(vcf@meta,"##R_custom_script=0.retain_non-identical_variants.R")

# subset original vcf to those rows and save
write.table(data.frame(original_length = original_length, non_identical_genotype_length = non_identical_genotype_length),
              file = paste0(vcf_path,"_Nvariants_retained.txt"),quote = F, row.names = F)
write.vcf(vcf,output_path)