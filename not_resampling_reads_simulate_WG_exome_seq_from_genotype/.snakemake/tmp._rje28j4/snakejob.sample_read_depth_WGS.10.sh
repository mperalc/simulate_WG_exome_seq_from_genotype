#!/bin/sh
# properties = {"type": "single", "rule": "sample_read_depth_WGS", "local": false, "input": ["../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"], "output": ["../../data/pool_19d/depth_df.txt.gz"], "wildcards": {"sample": "pool_19d"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "cores": "-n 1", "memory": "-M40000 -R'span[hosts=1] select[mem>40000] rusage[mem=40000]'", "jobname": "-o ../../logs/log_pool_19d_sample_read_depth_WGS.%J.%I", "error": "-e ../../errors/error_pool_19d_sample_read_depth_WGS.%J.%I"}, "log": [], "threads": 1, "resources": {}, "jobid": 10, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_19d/depth_df.txt.gz --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp._rje28j4 ../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules sample_read_depth_WGS --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp._rje28j4/10.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp._rje28j4/10.jobfailed"; exit 1)

