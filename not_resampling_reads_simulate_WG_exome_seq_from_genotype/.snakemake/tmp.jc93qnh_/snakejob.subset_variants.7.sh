#!/bin/sh
# properties = {"type": "single", "rule": "subset_variants", "local": false, "input": ["../../data/pool_19d/genotype/merged.genotype.hg38.vcf.gz"], "output": ["../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"], "wildcards": {"sample": "pool_19d"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "cores": "-n 1", "memory": "-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'", "jobname": "-o ../../logs/log_pool_19d_subset_variants_WGS.%J.%I", "error": "-e ../../errors/error_pool_19d_subset_variants_WGS.%J.%I"}, "log": [], "threads": 1, "resources": {}, "jobid": 7, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jc93qnh_ ../../data/pool_19d/genotype/merged.genotype.hg38.vcf.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules subset_variants --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jc93qnh_/7.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jc93qnh_/7.jobfailed"; exit 1)

