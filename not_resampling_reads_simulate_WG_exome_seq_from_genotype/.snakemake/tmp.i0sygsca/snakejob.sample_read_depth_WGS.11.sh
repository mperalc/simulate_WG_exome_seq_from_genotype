#!/bin/sh
# properties = {"type": "single", "rule": "sample_read_depth_WGS", "local": false, "input": ["../../data/pool_8d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"], "output": ["../../data/pool_8d/depth_df.txt.gz"], "wildcards": {"sample": "pool_8d"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "cores": "-n 1", "memory": "-M40000 -R'span[hosts=1] select[mem>40000] rusage[mem=40000]'", "jobname": "-o ../../logs/log_pool_8d_sample_read_depth_WGS.%J.%I", "error": "-e ../../errors/error_pool_8d_sample_read_depth_WGS.%J.%I"}, "log": [], "threads": 1, "resources": {}, "jobid": 11, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_8d/depth_df.txt.gz --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.i0sygsca ../../data/pool_8d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules sample_read_depth_WGS --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.i0sygsca/11.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.i0sygsca/11.jobfailed"; exit 1)

