#!/bin/sh
# properties = {"type": "single", "rule": "merge_and_sort_genotype_vcf", "local": false, "input": ["../../data/sampleMetadata.txt", "../../data/hipsci.vcf.files.txt"], "output": ["../../data/pool_20d_similar/genotype/merged.genotype.vcf.gz"], "wildcards": {"sample": "pool_20d_similar"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "threads": "-n 1", "memory": "-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'", "jobname": "-o ../../logs/log_pool_20d_similar_merge_and_sort_genotype_vcf.%J.%I", "error": "-e ../../errors/error_pool_20d_similar_merge_and_sort_genotype_vcf.%J.%I"}, "log": [], "threads": 1, "resources": {}, "jobid": 3, "cluster": {}}
cd /lustre/scratch123/hgi/mdt1/projects/otar2065/simulation_WGS_exome_data/code/not_resampling_reads_simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_20d_similar/genotype/merged.genotype.vcf.gz --snakefile /lustre/scratch123/hgi/mdt1/projects/otar2065/simulation_WGS_exome_data/code/not_resampling_reads_simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch123/hgi/mdt1/projects/otar2065/simulation_WGS_exome_data/code/not_resampling_reads_simulate_WG_exome_seq_from_genotype/.snakemake/tmp.qpgti_kh ../../data/sampleMetadata.txt ../../data/hipsci.vcf.files.txt --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules merge_and_sort_genotype_vcf --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch123/hgi/mdt1/projects/otar2065/simulation_WGS_exome_data/code/not_resampling_reads_simulate_WG_exome_seq_from_genotype/.snakemake/tmp.qpgti_kh/3.jobfinished" || (touch "/lustre/scratch123/hgi/mdt1/projects/otar2065/simulation_WGS_exome_data/code/not_resampling_reads_simulate_WG_exome_seq_from_genotype/.snakemake/tmp.qpgti_kh/3.jobfailed"; exit 1)

