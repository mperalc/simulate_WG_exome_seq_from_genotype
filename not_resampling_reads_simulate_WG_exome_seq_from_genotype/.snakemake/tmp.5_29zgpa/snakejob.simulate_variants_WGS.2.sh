#!/bin/sh
# properties = {"type": "single", "rule": "simulate_variants_WGS", "local": false, "input": ["../../data/pool_19d/genotype/merged.genotype.vcf.gz", "../../data/pool_19d/depth_df.txt.gz"], "output": ["../../data/pool_19d/b/b_donor_proportions_50_balance_part1.txt.gz", "../../data/pool_19d/b/b_estimate_donor_proportions_50_balance_part1.RData", "../../data/pool_19d/b/donor_proportions_50_balance.txt.gz", "../../data/pool_19d/b/genotype_minor_allele_dosage_part1.txt.gz"], "wildcards": {"sample": "pool_19d"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "threads": "-n 10", "memory": "-M320000 -R'span[hosts=1] select[mem>320000] rusage[mem=320000]'", "jobname": "-o ../../logs/log_simulate_variants_WGS.%J.%I", "error": "-e ../../errors/error_simulate_variants_WGS.%J.%I", "snp_chunks": "200000"}, "log": [], "threads": 1, "resources": {}, "jobid": 2, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_19d/b/b_estimate_donor_proportions_50_balance_part1.RData --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.5_29zgpa ../../data/pool_19d/genotype/merged.genotype.vcf.gz ../../data/pool_19d/depth_df.txt.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules simulate_variants_WGS --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.5_29zgpa/2.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.5_29zgpa/2.jobfailed"; exit 1)

