#!/bin/sh
# properties = {"type": "single", "rule": "estimate_weights", "local": false, "input": ["../../data/pool_20d_similar/b/b_estimate_donor_proportions_balance_trial_1.csv", "../../data/pool_20d_similar/b/genotype_minor_allele_dosage.csv"], "output": ["../../data/pool_20d_similar/b/w_estimate_donor_proportions_balance_5_trials.txt.gz", "../../data/pool_20d_similar/b/w_estimate_donor_proportions_imbalance_5_trials.txt.gz"], "wildcards": {"sample": "pool_20d_similar"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "cores": "-n 1", "memory": "-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'", "jobname": "-o ../../logs/log_pool_20d_similar_estimate_weights.%J.%I", "error": "-e ../../errors/error_pool_20d_similar_estimate_weights.%J.%I", "trials": "5"}, "log": [], "threads": 1, "resources": {}, "jobid": 18, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_20d_similar/b/w_estimate_donor_proportions_imbalance_5_trials.txt.gz --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jrtbcfni ../../data/pool_20d_similar/b/b_estimate_donor_proportions_balance_trial_1.csv ../../data/pool_20d_similar/b/genotype_minor_allele_dosage.csv --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules estimate_weights --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jrtbcfni/18.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.jrtbcfni/18.jobfailed"; exit 1)

