#!/bin/sh
# properties = {"type": "single", "rule": "subset_variants", "local": false, "input": ["../../data/pool_19d/genotype/merged.genotype.hg38.vcf.gz"], "output": ["../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"], "wildcards": {"sample": "pool_19d"}, "params": {"group": "-G teamtrynka", "queue": "-q normal", "cores": "-n 1", "memory": "-M40000 -R'span[hosts=1] select[mem>40000] rusage[mem=40000]'", "jobname": "-o ../../logs/log_subset_variants_WGS.%J.%I", "error": "-e ../../errors/error_subset_variants_WGS.%J.%I"}, "log": [], "threads": 1, "resources": {}, "jobid": 3, "cluster": {}}
cd /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype && \
/software/teamtrynka/conda/trynka-base/bin/python \
-m snakemake ../../data/pool_19d/genotype/merged.genotype.hg38.nonIdentical.vcf.gz --snakefile /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.6ykyhgtt ../../data/pool_19d/genotype/merged.genotype.hg38.vcf.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules subset_variants --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.6ykyhgtt/3.jobfinished" || (touch "/lustre/scratch117/cellgen/teamtrynka/marta/simulation_WGS_exome_data/code/simulate_WG_exome_seq_from_genotype/.snakemake/tmp.6ykyhgtt/3.jobfailed"; exit 1)

