#!/usr/bin/env python3
# Snakefile for predicting sequencing depth needed for calculating donor proportions in pools using WGS/exome-seq
# snakemake is in /software/teamtrynka/conda/trynka-base/bin/snakemake
import os

SAMPLE = ["pool_19d","pool_8d","pool_20d_similar"]

rule all:
    input:
        genotype=expand("../../data/{sample}/genotype/merged.genotype.vcf.gz", sample=SAMPLE),
        genotype_hg38=expand("../../data/{sample}/genotype/merged.genotype.hg38.vcf.gz", sample=SAMPLE),
        subset=expand("../../data/{sample}/genotype/merged.genotype.hg38.nonIdentical.vcf.gz",sample=SAMPLE),
        depth = expand("../../data/{sample}/depth_df.txt.gz", sample=SAMPLE),
        b_estimate=expand("../../data/{sample}/b/b_estimate_donor_proportions_balance_trial_1.csv", sample=SAMPLE),
        weight_imbalanced=expand("../../data/{sample}/b/w_estimate_donor_proportions_imbalance_5_trials.txt.gz",sample=SAMPLE)

rule merge_and_sort_genotype_vcf:
    input:
        metadata="../../data/sampleMetadata.txt",
        all_hipsci_vcfs="../../data/hipsci.vcf.files.txt"
    output:
        genotype="../../data/{sample}/genotype/merged.genotype.vcf.gz"
    message: "Merging sample genotype files present in the metadata file needed for next step. Run with snakemake --jobs 3 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        threads="-n 1",
        memory="-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'",
        jobname= "-o ../../logs/log_{sample}_merge_and_sort_genotype_vcf.%J.%I",
        error="-e ../../errors/error_{sample}_merge_and_sort_genotype_vcf.%J.%I"
    shell:
        """
        echo "Subsetting file names to merge, from all hipsci files available looking at the metadata IDs"
        detectedDonors="../../data/{wildcards.sample}/genotype/{wildcards.sample}_donorNames.txt"
        cat {input.metadata} | grep {wildcards.sample} | awk '{{print $3}}' | tr ';' '\n' > $detectedDonors
        # Subset all hipsci files
        while read ptn; do grep $ptn {input.all_hipsci_vcfs}; done < ../../data/{wildcards.sample}/genotype/{wildcards.sample}_donorNames.txt > ../../data/{wildcards.sample}/genotype/{wildcards.sample}_hipsci_subset.txt
        echo "Merging VCFs"
        /software/teamtrynka/conda/trynka-base/bin/bcftools merge --file-list ../../data/{wildcards.sample}/genotype/{wildcards.sample}_hipsci_subset.txt -Oz -o ../../data/{wildcards.sample}/genotype/temp.vcf.gz

        echo "Saving VCF header"
        zcat ../../data/{wildcards.sample}/genotype/temp.vcf.gz | awk '/^\#/' > ../../data/{wildcards.sample}/genotype/header.txt

        echo "Sorting"
        /software/teamtrynka/conda/trynka-base/bin/bedtools sort -i ../../data/{wildcards.sample}/genotype/temp.vcf.gz > {output.genotype}

        echo "Attaching header"
        cat {output.genotype} >> ../../data/{wildcards.sample}/genotype/header.txt
        mv ../../data/{wildcards.sample}/genotype/header.txt {output.genotype}

        echo "Changing sample names in header"
        /software/teamtrynka/conda/trynka-base/bin/bcftools reheader --samples $detectedDonors -o ../../data/{wildcards.sample}/genotype/temp_reheaded.vcf {output.genotype}

        echo "Compressing with bgzip"
        # Genotype files need to be compressed with bgzip to work with Vireo
        /software/teamtrynka/conda/trynka-base/bin/bgzip ../../data/{wildcards.sample}/genotype/temp_reheaded.vcf
        mv ../../data/{wildcards.sample}/genotype/temp_reheaded.vcf.gz {output.genotype}

        echo "Generating index file"
        /software/teamtrynka/conda/trynka-base/bin/bcftools index {output.genotype}
        #rm ../../data/{wildcards.sample}/genotype/temp.vcf.gz
        """
rule liftover_hg19_hg38:
    input:
        genotype="../../data/{sample}/genotype/merged.genotype.vcf.gz"
    output:
        genotype="../../data/{sample}/genotype/merged.genotype.hg38.vcf.gz"
    message: "Lifting over the VCF from hg19 (GRch37) to hg38 (GRch38). Run with snakemake --jobs 1 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        threads="-n 1",
        memory="-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'",
        jobname= "-o ../../logs/log_{sample}_liftover_hg19_hg38.%J.%I",
        error="-e ../../errors/error_{sample}_liftover_hg19_hg38.%J.%I",
	chain="/lustre/scratch123/hgi/teams/trynka/marta/resources/for_liftOver/chain/hg19ToHg38.over.chain.gz",
	fasta="/lustre/scratch123/hgi/teams/trynka/marta/resources/for_liftOver/fasta/hg38.fa.gz",
	dict="/lustre/scratch123/hgi/teams/trynka/marta/resources/for_liftOver/fasta/hg38.dict",
	reference="/lustre/scratch115/teams/bassett/Erica/Miseq_10x_iPSC_082019/refdata-cellranger-GRCh38-3.0.0/fasta/genome.fa",
	recoding="/lustre/scratch123/hgi/teams/trynka/marta/resources/for_liftOver/chromosome_mappings_hg19_to_b37/GRCh37_UCSC2ensembl_b37tohg19.txt"
    shell:
        """
	#echo "Creating dictionary..."
	#/software/teamtrynka/picard.jar CreateSequenceDictionary R={params.fasta} O={params.dict}

	echo "Changing vcf from bg37 to hg19"
	/software/teamtrynka/conda/trynka-base/bin/bcftools annotate --rename-chrs {params.recoding} -o ../../data/{wildcards.sample}/genotype/temp_chrChange.vcf {input.genotype}

    echo "Lifting over..."
	/software/teamtrynka/picard.jar LiftoverVcf \
	I=../../data/{wildcards.sample}/genotype/temp_chrChange.vcf \
	O=../../data/{wildcards.sample}/genotype/temp_liftedOver.vcf \
	CHAIN={params.chain} \
	REJECT=../../data/{wildcards.sample}/genotype/rejected_variants.vcf \
	R={params.fasta}


    echo "Generating index file"
    /software/teamtrynka/conda/trynka-base/bin/bcftools view -Oz -o {output.genotype} ../../data/{wildcards.sample}/genotype/temp_liftedOver.vcf
    /software/teamtrynka/conda/trynka-base/bin/bcftools index {output.genotype}
    rm ../../data/{wildcards.sample}/genotype/temp_chrChange.vcf

        """
rule subset_variants:
    input:
        genotype="../../data/{sample}/genotype/merged.genotype.hg38.vcf.gz"
    output:
        genotype="../../data/{sample}/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"
    message: "Subsetting to variants that have non-identical genotypes across donors. Run with snakemake --jobs 3 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        threads="-n 1",
        memory="-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'",
        jobname= "-o ../../logs/log_{sample}_subset_variants_WGS.%J.%I",
        error="-e ../../errors/error_{sample}_subset_variants_WGS.%J.%I"
    shell:
        """
        echo "Subsetting to variants that have non-identical genotypes across donors"
        /lustre/scratch118/humgen/resources/conda_envs/R.4/bin/Rscript 0.Retain_non-identical_variants.R {input.genotype} {output.genotype}

        # Decompressing and re-compressing in right format
        gunzip -d {output.genotype}
        /software/teamtrynka/conda/trynka-base/bin/bgzip ../../data/{wildcards.sample}/genotype/merged.genotype.hg38.nonIdentical.vcf

        echo "Generating index file"
        /software/teamtrynka/conda/trynka-base/bin/bcftools index {output.genotype}
        """

rule sample_read_depth_WGS:
    input:
        genotype="../../data/{sample}/genotype/merged.genotype.hg38.nonIdentical.vcf.gz"
    output:
        depth="../../data/{sample}/depth_df.txt.gz"
    message: "Sampling read depth at different coverages for SNPs present in genotype file. Number of snps to process in snp_chunks Run with snakemake --jobs 3 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        threads="-n 1",
        memory="-M40000 -R'span[hosts=1] select[mem>40000] rusage[mem=40000]'",
        jobname= "-o ../../logs/log_{sample}_sample_read_depth_WGS.%J.%I",
        error="-e ../../errors/error_{sample}_sample_read_depth_WGS.%J.%I"
    shell:
        """
        echo "Counting number of variants in VCF"
        nvariants=$(/software/teamtrynka/conda/trynka-base/bin/bcftools index --nrecords {input.genotype})

        echo "Sampling local read depth from Poisson distribution, with mean = range of coverages (0.1, 0.25, 0.5 ...)"
        ## Takes as input the number of variants to cover, and as output the dataframe with reads (row) sampled per coverage (column)
        /lustre/scratch118/humgen/resources/conda_envs/R.4/bin/Rscript 1.sample_read_depth_WGS.R $nvariants {output.depth}
        """

rule simulate_variants_WGS:
    input:
        genotype="../../data/{sample}/genotype/merged.genotype.hg38.nonIdentical.vcf.gz",
	depth="../../data/{sample}/depth_df.txt.gz"
    output:
        b="../../data/{sample}/b/b_donor_proportions_balance_5_trials.txt.gz",
        b_estimate="../../data/{sample}/b/b_estimate_donor_proportions_balance_trial_1.csv",
        donor_prop="../../data/{sample}/b/donor_proportions_balance_5_trials.txt.gz",
        gt_dosage="../../data/{sample}/b/genotype_minor_allele_dosage.csv"
    message: "Simulating the exome variants from the genotypes and sampled number of reads. Run with snakemake --jobs 1 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q long",
        threads="-n 8",
        memory="-M320000 -R'span[hosts=1] select[mem>320000] rusage[mem=320000]'",
        jobname= "-o ../../logs/log_{sample}_simulate_variants_WGS.%J.%I",
        error="-e ../../errors/error_{sample}_simulate_variants_WGS.%J.%I",
        trials="5"
    shell:
        """
        echo "Counting number of variants in VCF"
        nvariants=$(/software/teamtrynka/conda/trynka-base/bin/bcftools index --nrecords {input.genotype})

        echo "Simulating donor porportions, and variants and minor allele frequencies from the genotype and proportions."
        echo "Donor proportions: random balanced (random) and imbalanced (imbalanced)"
        echo "Takes as input the number of variants to cover in total, the number of variants to process in each loop (memory restrictions), the depth dataframe generated before, the genotype vcf and the donor pool id."
        /lustre/scratch118/humgen/resources/conda_envs/R.4/bin/Rscript 2.simulate_variants_WGS.R $nvariants {params.trials} {input.depth} {input.genotype} {wildcards.sample}
        """

rule estimate_weights:
    input:
        b_estimate="../../data/{sample}/b/b_estimate_donor_proportions_balance_trial_1.csv",
	gt_dosage="../../data/{sample}/b/genotype_minor_allele_dosage.csv"
    output:
        weight_balanced="../../data/{sample}/b/w_estimate_donor_proportions_balance_5_trials.txt.gz",
        weight_imbalanced="../../data/{sample}/b/w_estimate_donor_proportions_imbalance_5_trials.txt.gz"
    message: "Calculating donor proportions (weights) from estimated minor allele frequency and genotype file. Run with snakemake --jobs 1 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        threads="-n 1",
        memory="-M100000 -R'span[hosts=1] select[mem>100000] rusage[mem=100000]'",
        jobname= "-o ../../logs/log_{sample}_estimate_weights.%J.%I",
        error="-e ../../errors/error_{sample}_estimate_weights.%J.%I",
        trials="5"
    shell:
        """
	echo "Calculating donor proportions (weights) from estimated minor allele frequency and genotype file."
        echo "Takes as input the donor id. Also uses the genotype and minor allele frequency files generated in the previous step (no need to pass them as arguments)."
        /lustre/scratch118/humgen/resources/conda_envs/R.4/bin/Rscript 3.estimate_weights.R {wildcards.sample} {params.trials}
        """
