# examine real-life read depth
library(data.table)

bam_readcount_path_1= "../../data/media_bam_readcount_subset/bam_readcounts_littlebam_noQfilter.txt"
bam_readcount_path_2= "../../data/media_bam_readcount_subset/test_bam-readcount.txt"

bam_readcount_1= data.table::fread( cmd = paste("cat", bam_readcount_path_1,"| awk -F '\t' '{print $1 , $2 , $3 , $4, $5, $6, $7, $8, $9, $10}'"))
colnames(bam_readcount_1) = c("CHROM","POS","REF","TOTAL_READS","DEL","A","C","G","T","N")

bam_readcount_2= data.table::fread( cmd = paste("cat", bam_readcount_path_2,"| awk -F '\t' '{print $1 , $2 , $3 , $4, $5, $6, $7, $8, $9, $10}'"))
colnames(bam_readcount_2) = c("CHROM","POS","REF","TOTAL_READS","DEL","A","C","G","T","N")

sum(bam_readcount_1$TOTAL_READS) / nrow(bam_readcount_1)
table(bam_readcount_1$TOTAL_READS)
hist(bam_readcount_1$TOTAL_READS,                                          
     breaks = 100,
     main = "")

sum(bam_readcount_2$TOTAL_READS) / nrow(bam_readcount_2)
table(bam_readcount_2$TOTAL_READS)
hist(bam_readcount_2$TOTAL_READS,                                          
     breaks = 100,
     main = "")

# examine my simulated read depth

simulation = data.table::fread("../../data/pool_19d/depth_df.txt.gz", header = T)

colSums(simulation) / nrow(simulation)

# looks ok
diff = abs(as.numeric(names(colSums(simulation) )) - (colSums(simulation) / nrow(simulation)))
sum(diff > 0.01) > 1


# Calculating breadth of coverage (% of bases with at least 1 count from the total)

(colSums(simulation>1) / nrow(simulation)) * 100
# these are lower than real-life estimates
#https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-3164-z   fig 4
hist(simulation$`5`)
hist(simulation$`0.01`)
hist(simulation$`0.1`)
